edge浏览器主页被篡改如何修改
edge浏览器主页被篡改怎么办？edge浏览器主页怎么设置？window 10更新也有一阵子了，大家用的还习惯么？相较于win 7，win10很多地方都有了很大的改动，比如微软的edge浏览器，下面浏览器家园小编就为大家介绍一下edge浏览器怎么设置主页
首先我们打开edge浏览器点击右上角的“...”菜单按钮，然后打开点击设置
edge浏览器主页被篡改如何修改

![Browser Desktop浏览器新标签页截图](image/1.jpg)

进入菜单设置时候，然后先下滑动，找到高级设置

edge浏览器主页被篡改如何修改
![Browser Desktop浏览器新标签页截图](image/2.png)
![Browser Desktop浏览器新标签页截图](image/3.png)
在这里有一个主页开关，默认是关闭的，你可以点击打开，然后把主页地址放进去，点击保存

edge浏览器主页被篡改如何修改
![Browser Desktop浏览器新标签页截图](image/4.png)
![Browser Desktop浏览器新标签页截图](image/6.png)

嘿嘿，看小编的教程，这个设置主页的地方你肯定能找到了吧